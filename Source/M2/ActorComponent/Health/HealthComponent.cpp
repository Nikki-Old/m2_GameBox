// Fill out your copyright notice in the Description page of Project Settings.


#include "ActorComponent/Health/HealthComponent.h"
#include "WorldActor/Damage/DamageActorBase.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...

	InititalHealthComponent();
}

void UHealthComponent::InititalHealthComponent()
{
	// Bind to Owner "TakeAnyDamage"
	if (!GetOwner())
		return;

	// Bind Owner "OnTakeAnyDamage" and "OwnerTakeAnyDamage" function:
	auto OwnerActor = Cast<AActor>(GetOwner());
	if (OwnerActor)
	{
		OwnerActor->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::OwnerTakeAnyDamage);
		OwnerActor->OnTakePointDamage.AddDynamic(this, &UHealthComponent::OwnerTakePointDamage);
		OwnerActor->OnTakeRadialDamage.AddDynamic(this, &UHealthComponent::OwnerTakeRadialDamage);
	}
}

// Called every frame
void UHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UHealthComponent::ChangeHealth(const float Change)
{
	if (bIsDead || Change == 0.f)
		return;

	// Change Health:
	Health += Change;

	OnChangeHealth.Broadcast(GetOwner(), Health, Change);

	if (Change < 0 && bIsNeedSpawnDamage)
	{
		SpawnDamageActor(HealthDamageClass, Change);
	}

	if (Health > 100.0f)
	{
		Health = 100.0f;
	}

	if (Health <= 0)
	{
		bIsDead = true;
		OnDead.Broadcast(LastDamageCauser);
	}
}

void UHealthComponent::OwnerTakeAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	Damage *= DamageCoef;
	LastDamageCauser = DamageCauser;
	ChangeHealth(-Damage);
}

void UHealthComponent::OwnerTakePointDamage(AActor* DamagedActor, float Damage, AController* InstigatedBy, FVector HitLocation, UPrimitiveComponent* FHitComponent, FName BoneName, FVector ShotFromDirection, const UDamageType* DamageType, AActor* DamageCauser)
{
	if (FMath::IsNearlyZero(Damage) && !DamageCauser)
	{
		return;
	}
}

void UHealthComponent::OwnerTakeRadialDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, FVector Origin, FHitResult HitInfo, AController* InstigatedBy, AActor* DamageCauser)
{
	if (FMath::IsNearlyZero(Damage) && !DamageCauser)
	{
		return;
	}
}

void UHealthComponent::SpawnDamageActor(TSubclassOf<ADamageActorBase> DamageClass, const float Change)
{
	if (!GetWorld() && !IsValid(DamageClass))
		return; // TO DO Check();

	auto SpawnTransform = this->GetOwner()->GetTransform();

	auto DamageActor = (GetWorld()->SpawnActor<ADamageActorBase>(DamageClass, SpawnTransform));

	if (DamageActor)
	{
		// Add offset:
		auto OldLocation = this->GetOwner()->GetActorLocation();
		DamageActor->SetActorLocation(OldLocation + DamageOffsetLocation);
		DamageActor->UpdateDamageWidget(GetHealth(), Change);
	}
}


