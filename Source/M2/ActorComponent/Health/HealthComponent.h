// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

/** For broadcast Current Health and Change: */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnChangeHealth, AActor*, Owner, float, CurrentHealth, float, Change);

/** Broadcast On Dead: */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnDead, AActor*, LastDamageCauser);
class ADamageActorBase;

UCLASS(Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class M2_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UHealthComponent();

	// Delegates:
	UPROPERTY(BlueprintAssignable, Category = "Health")
	FOnChangeHealth OnChangeHealth;

	/** For Owner, can bind this broadcast and Owner custom function: */
	UPROPERTY(BlueprintAssignable, Category = "Health")
	FOnDead OnDead;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	/** Initial Function: */
	virtual void InititalHealthComponent();

	/** Healh: */
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Health")
	float Health = 100.0f;

	/**Is Dead*/
	bool bIsDead = false;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	///* Particles on Damage : */
	//UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Health | FX")
	//TArray<TSoftObjectPtr<UParticleSystem>> DamageParticles;

	/* Decals after DamageParticle :*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Health | FX")
	UMaterial* DamageDecalMaterial = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Health | FX")
	float LifeDamageDecal = 1.0f;

	/** If Need Spawn Damage widget, set TRUE: */
	UPROPERTY(EditDefaultsOnly, Category = "Health | Damage")
	bool bIsNeedSpawnDamage = false;

	/** Offset spawn location */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health | Damage", meta = (EditCondition = "bIsNeedSpawnDamage"))
	FVector DamageOffsetLocation = FVector(0);

	/** Current Damage * DamageCoef = Change Health */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health | Damage")
	float DamageCoef = 1.0f;

	/** Class Damage Actor: */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Health | Damage", meta = (EditCondition = "bIsNeedSpawnDamage"))
	TSubclassOf<ADamageActorBase> HealthDamageClass;

	// Healts:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Health")
	float GetHealth() const { return Health; }

	UFUNCTION(BlueprintCallable, Category = "Health")
	virtual void ChangeHealth(const float Change);

	// Dead:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Health")
	bool IsDead() const { return bIsDead; }

protected:
	void SpawnDamageActor(TSubclassOf<ADamageActorBase> DamageClass, const float Change);

	bool bIsSpawnDamageParticle = false;

	/** Bind this function and Owner Delegate - "OnTakeAnyDamage". */
	UFUNCTION()
	virtual void OwnerTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	/** Bind this function and Owner Delegate - "OnTakePointDamage". */
	UFUNCTION()
	virtual void OwnerTakePointDamage(AActor* DamagedActor, float Damage, class AController* InstigatedBy, FVector HitLocation, class UPrimitiveComponent* FHitComponent, FName BoneName, FVector ShotFromDirection, const class UDamageType* DamageType, AActor* DamageCauser);

	/** Bind this function and Owner Delegate - "OnTakeRadialDamage". */
	UFUNCTION()
	virtual void OwnerTakeRadialDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, FVector Origin, FHitResult HitInfo, class AController* InstigatedBy, AActor* DamageCauser);

private:
	UPROPERTY()
	AActor* LastDamageCauser = nullptr;
};
