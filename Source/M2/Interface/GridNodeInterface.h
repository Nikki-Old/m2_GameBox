// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "FuncLibrary/M2_Types.h"
#include "GridNodeInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UGridNodeInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class M2_API IGridNodeInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "GridNode")
	void SetGridNodeInformation(FGridNodeInfo NewInformation);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "GridNode")
	bool IsActive();
};
