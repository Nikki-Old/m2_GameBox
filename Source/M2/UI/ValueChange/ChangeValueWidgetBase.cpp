// Fill out your copyright notice in the Description page of Project Settings.

#include "UI/ValueChange/ChangeValueWidgetBase.h"
#include "Components/TextBlock.h"
#include "Kismet/KismetTextLibrary.h"
#include "Kismet/KismetMathLibrary.h"

//#include "FuncLibrary/Constant/TPSConstant.h"
#include "WorldActor/Damage/DamageActorBase.h"

bool UChangeValueWidgetBase::Initialize()
{
	return Super::Initialize();
}

void UChangeValueWidgetBase::NativeConstruct()
{
	Super::NativeConstruct();

	if (TXTValueChange)
	{
		TXTValueChange->TextDelegate.BindUFunction(this, TEXT("GetValueChangeTXT"));
		TXTValueChange->ColorAndOpacityDelegate.BindUFunction(this, TEXT("GetColorTXTValueChange"));
	}

	ValueWidgetAnimIsEnd.BindDynamic(this, &UChangeValueWidgetBase::AnimationIsEnd);

	// Bind to AnimationFinished Delegate and AnimationIsEnd:
	if (TXTValueChangeAnim)
	{
		BindToAnimationFinished(TXTValueChangeAnim, ValueWidgetAnimIsEnd);
	}
}

void UChangeValueWidgetBase::InitEvent(AActor* Owner, float Change)
{
	checkf(Owner, TEXT("Owner is NULL"));

	// Set Owneractor:
	OwnerActor = Cast<ADamageActorBase>(Owner);

	check(OwnerActor);

	/* Save current Change: **/
	SetValueChangeInTextBlock(Change);
	/* Set Color for TXT: **/
	TXTValueChange->ColorAndOpacity = GetColorTXTValueChange();

	// Play animation:
	if (TXTValueChangeAnim)
	{
		PlayAnimation(TXTValueChangeAnim);
	}
}

void UChangeValueWidgetBase::SetValueChangeInTextBlock(float Change)
{
	ValueChange = Change;
}

FText UChangeValueWidgetBase::GetValueChangeTXT() const
{
	TEnumAsByte<ERoundingMode> RoundingMode = ERoundingMode::HalfFromZero;
	return UKismetTextLibrary::Conv_FloatToText(ValueChange, RoundingMode, true, true, 1, 3, 0, 0);
}

void UChangeValueWidgetBase::AnimationIsEnd()
{
	if (OwnerActor)
		OwnerActor->DestroyEvent();
}

/* FLinearColor UTPSChangeHealthWidgetBase::GetColorHealthChange() const
{
	return HealthChangeColor;
}*/

FSlateColor UChangeValueWidgetBase::GetColorTXTValueChange()
{
	if (ValueChange == 0.0f || !OwnerActor)
		return FSlateColor(BaseColor);

	float CurrentHeath = OwnerActor->GetLeftValue();

	// Improved, inctorrect color calcuclations. Must be GREEN to RED:
	BaseColor.R = UKismetMathLibrary::MapRangeClamped(CurrentHeath, 0.0f, 100.0f, 1.0f, 0.0f);
	BaseColor.G = UKismetMathLibrary::MapRangeClamped(CurrentHeath, 0.0f, 100.0f, 0.0f, 1.0f);
		
	BaseColor.B = 0.0f;
	return FSlateColor(BaseColor);
}