// Fill out your copyright notice in the Description page of Project Settings.

#include "UI/Grid/GridWidget.h"
#include "Interface/GridNodeInterface.h"

void UGridWidget::GetNotActiveIndex(TArray<int32>& FreeIndexes) const
{
	for (int32 i = 0; i < GridNodes.Num(); i++)
	{
		if (GridNodes.IsValidIndex(i) && GridNodes[i])
		{
			if (GridNodes[i]->GetClass()->ImplementsInterface(UGridNodeInterface::StaticClass()))
			{
				if (!IGridNodeInterface::Execute_IsActive(GridNodes[i]))
				{
					FreeIndexes.Add(i);
				}
			}
		}
	}
}
