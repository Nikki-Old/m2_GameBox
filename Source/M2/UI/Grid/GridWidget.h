// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GridWidget.generated.h"

/**
 * 
 */
UCLASS()
class M2_API UGridWidget : public UUserWidget
{
	GENERATED_BODY()

public:
protected:
	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Grid")
	TArray<UUserWidget*> GridNodes;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Grid | Sort")
	void GetNotActiveIndex(TArray<int32>& FreeIndexes) const;

	//UFUNCTION(BlueprintPure, Category = "Grid")
	//int32 GetFirstFreeIndex() const { return CountFreeGridNode; }

	//UFUNCTION(BlueprintCallable, Category = "Grid")
	//void IncrementCountFreeGridNodes() { CountFreeGridNode--; }

private:
};
