// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"

#include "M2_Types.generated.h"

/**
 * 
 */
class UTexture2D;
class AM2WarriorBase;

UENUM(BlueprintType)
enum class ETeamType : uint8
{
	None_Type UMETA(DisplayName = "None"),
	Red_Type UMETA(DisplayName = "Red Team"),
	Green_Type UMETA(DisplayName = "Green Team")
};

UENUM(BlueprintType)
enum class ECastleControllerType : uint8
{
	None_Type UMETA(DisplayName = "None"),
	Player_Type UMETA(DisplayName = "Player"),
	AI_Type UMETA(DisplayName = "AI")
};

UENUM(BlueprintType)
enum class EGridNodeState : uint8
{
	None_State UMETA(DisplayName = "None"),
	Active_State UMETA(DisplayName = "Active"),
	NotActive_State UMETA(DisplayName = "Not Active")
};

USTRUCT(BlueprintType)
struct FGridNodeInfo : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GridNode")
	EGridNodeState State = EGridNodeState::None_State;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GridNode")
	UTexture2D* Image = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GridNode")
	FName DisplayName = "";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GridNode")
	FName TableWarriorName = "";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GridNode")
	int32 Count = 0;
};

USTRUCT(BlueprintType)
struct FWarriorInfo
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WarriorInfo")
	FName DisplayName = "";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WarriorInfo")
	FName WarriorTableName = "";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WarriorInfo")
	float HealthDamageCoef = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WarriorInfo | Attack")
	float AttackDamage = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WarriorInfo | Attack")
	float AttackDistance = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WarriorInfo | Attack")
	float AttackRateMin = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WarriorInfo | Attack")
	float AttackRateMax = 0.0f;

	void MultiplyOnGridCount(const int32 GridCount)
	{
		AttackDamage *= GridCount;
		AttackDistance *= GridCount;
		HealthDamageCoef -= GridCount * 0.1;
		AttackRateMin *= GridCount;
		AttackRateMax *= GridCount;
	}
};

USTRUCT(BlueprintType)
struct FTableWarriorInfo : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WarriorInfo")
	TSubclassOf<AM2WarriorBase> WarriorClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WarriorInfo")
	FWarriorInfo WarriorInfo;
};

UCLASS()
class M2_API UM2_Types : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
};
