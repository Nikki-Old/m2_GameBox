// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "M2_FunctionLibrary.generated.h"

/**
 * 
 */

UCLASS()
class M2_API UM2_FunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
	public:
	UFUNCTION(BlueprintPure, Category = "Health")
	static bool IsDead(const AActor* Actor);
};
