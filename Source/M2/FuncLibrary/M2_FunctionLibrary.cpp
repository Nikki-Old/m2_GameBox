// Fill out your copyright notice in the Description page of Project Settings.


#include "FuncLibrary/M2_FunctionLibrary.h"
#include "ActorComponent/Health/HealthComponent.h"

bool UM2_FunctionLibrary::IsDead(const AActor* Actor)
{
	if (!Actor)
	{
		return false;
	}
	
	bool Result = false;

	const auto HealthComponent = Cast<UHealthComponent>(Actor->GetComponentByClass(UHealthComponent::StaticClass()));
	if (HealthComponent)
	{
		return HealthComponent->IsDead();
	}

	return Result;
}