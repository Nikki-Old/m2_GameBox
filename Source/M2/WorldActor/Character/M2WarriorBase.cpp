// Fill out your copyright notice in the Description page of Project Settings.

#include "WorldActor/Character/M2WarriorBase.h"
#include "Game/M2GameInstance.h"
#include "Kismet/KismetMathLibrary.h"

AM2WarriorBase::AM2WarriorBase()
{
	AttackRate_Delegate.BindUFunction(this, "SetCanAttack", true);
}

void AM2WarriorBase::InitWarrior(const FWarriorInfo NewWarriorInfo, ETeamType TeamType, AActor* TargetCastle)
{
	WarriorInfo = NewWarriorInfo;

	if (TeamType != ETeamType::None_Type)
	{
		WarriorTeamType = TeamType;
	}

	UpdateWarriorTargetCastle(TargetCastle);
}

void AM2WarriorBase::UpdateWarriorInfo_Implementation()
{
}

void AM2WarriorBase::UpdateWarriorTargetCastle_Implementation(AActor* NewTargetCastle)
{
	if (NewTargetCastle)
	{
		WarriorTargetCastle = NewTargetCastle;
	}
}

void AM2WarriorBase::SetWarriorState(EWarriorState NewWarriorState)
{
	switch (NewWarriorState)
	{
		case EWarriorState::MoveToCastle_State:
			WarriorState = EWarriorState::MoveToCastle_State;
			break;

		case EWarriorState::FightWithWarrior_State:
			WarriorState = EWarriorState::FightWithWarrior_State;
			break;

		default:
			WarriorState = EWarriorState::None_State;
			break;
	}
}

bool AM2WarriorBase::AttackTarget_Implementation(AActor* Target)
{
	if (bIsCanAttack)
	{
		if (Target && GetWorld())
		{
			// Current distance between this character and Target:
			const auto Distance = UKismetMathLibrary::Vector_Distance(this->GetActorLocation(), Target->GetActorLocation());

			if (Distance <= WarriorInfo.AttackDistance)
			{
				Target->TakeDamage(WarriorInfo.AttackDamage, FDamageEvent(), nullptr, this);

				bIsCanAttack = false;

				const auto RandAttackRate = FMath::RandRange(WarriorInfo.AttackRateMin, WarriorInfo.AttackRateMax);
				GetWorld()->GetTimerManager().SetTimer(AttackDelay_Timer, AttackRate_Delegate, RandAttackRate, false);

				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	return false;
}
