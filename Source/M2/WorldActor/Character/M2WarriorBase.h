// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WorldActor/Character/CharacterBase.h"
#include "FuncLibrary/M2_Types.h"
#include "M2WarriorBase.generated.h"

/**
 * 
 */
UENUM(BlueprintType)
enum class EWarriorState : uint8
{
	None_State UMETA(DisplayName = "None"),
	MoveToCastle_State UMETA(DisplayName = "MoveToCastle"),
	FightWithWarrior_State UMETA(DisplayName = "FightWithWarrior")
};

UCLASS()
class M2_API AM2WarriorBase : public ACharacterBase
{
	GENERATED_BODY()

public:
	AM2WarriorBase();

	UFUNCTION(BlueprintCallable, Category = "Warrior")
	void InitWarrior(const FWarriorInfo NewWarriorInfo, ETeamType TeamType, AActor* TargetCastle);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Warrior")
	FWarriorInfo GetWarriorInfo() const { return WarriorInfo; }

protected:
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Warrior")
	ETeamType WarriorTeamType = ETeamType::None_Type;

	FWarriorInfo WarriorInfo;

	UFUNCTION(BlueprintNativeEvent, Category = "Warrior")
	void UpdateWarriorInfo();
	void UpdateWarriorInfo_Implementation();

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = Warrior)
	AActor* WarriorTargetCastle = nullptr;

	UFUNCTION(BlueprintNativeEvent, Category = "Warrior")
	void UpdateWarriorTargetCastle(AActor* NewTargetCastle);
	void UpdateWarriorTargetCastle_Implementation(AActor* NewTargetCastle);

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Warrior")
	EWarriorState WarriorState;

	UFUNCTION(BlueprintCallable, Category = "Warrior")
	void SetWarriorState(EWarriorState NewWarriorState);

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Warrior")
	bool bIsCanAttack = true;

	UFUNCTION(BlueprintCallable, Category = "Warrior")
	void SetCanAttack(bool Can) { bIsCanAttack = Can; }

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Warrior")
	bool AttackTarget(AActor* Target);
	bool AttackTarget_Implementation(AActor* Target);


private:
	FTimerHandle AttackDelay_Timer;
	FTimerDelegate AttackRate_Delegate;
};
