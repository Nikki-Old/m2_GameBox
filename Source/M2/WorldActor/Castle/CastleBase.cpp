// Fill out your copyright notice in the Description page of Project Settings.

#include "WorldActor/Castle/CastleBase.h"
#include "WorldActor/Character/M2WarriorBase.h"
#include "Game/M2GameInstance.h"

// Sets default values
ACastleBase::ACastleBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void ACastleBase::SpawnWarrior(const FName TableWarriorName, const int32 GridCount, ETeamType TeamType, AActor* TargetCastle, FVector SpawnLocation, FRotator SpawnRotation, APawn* InstigatorWarrior)
{
	if (GetWorld())
	{
		if (!M2GameInstance)
		{
			return;
		}

		FTableWarriorInfo TableWarriorInfo;
		if (!M2GameInstance->GetTableWarriorInfoByName(TableWarriorName, TableWarriorInfo))
		{
			return;
		}
		
		TableWarriorInfo.WarriorInfo.MultiplyOnGridCount(GridCount);

		FTransform SpawnTransform;
		SpawnTransform.SetLocation(SpawnLocation);
		SpawnTransform.SetRotation(FQuat(SpawnRotation));
		SpawnTransform.SetScale3D(FVector(1.0f * GridCount)); // GridCount!

		ESpawnActorCollisionHandlingMethod SpawnActorCollisionHandlingMethod = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		auto NewWarrior = GetWorld()->SpawnActorDeferred<AM2WarriorBase>(TableWarriorInfo.WarriorClass, SpawnTransform, this, InstigatorWarrior);
		if (NewWarrior)
		{
			NewWarrior->InitWarrior(TableWarriorInfo.WarriorInfo, TeamType, TargetCastle);
			NewWarrior->FinishSpawning(SpawnTransform);
			SpawnedWarriors.AddUnique(NewWarrior);
		}
	}
}

// Called when the game starts or when spawned
void ACastleBase::BeginPlay()
{
	Super::BeginPlay();

	// Get M2 GameInstance:

	M2GameInstance = Cast<UM2GameInstance>(GetGameInstance());
}

// Called every frame
void ACastleBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
