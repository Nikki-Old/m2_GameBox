// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FuncLibrary/M2_Types.h"
#include "CastleBase.generated.h"

class AM2WarriorBase;
class UM2GameInstance;

UCLASS()
class M2_API ACastleBase : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ACastleBase();
	UFUNCTION(BlueprintCallable, Category = "Castle")
	void SpawnWarrior(const FName TableWarriorName, const int32 GridCount, ETeamType TeamType, AActor* TargetCastle, FVector SpawnLocation, FRotator SpawnRotation, APawn* InstigatorWarrior);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Castle")
	TArray<AActor*> SpawnedWarriors;

	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "Castle")
	UM2GameInstance* M2GameInstance = nullptr;
};
