// Fill out your copyright notice in the Description page of Project Settings.

#include "Game/M2GameInstance.h"

bool UM2GameInstance::GetTableWarriorInfoByName(const FName WarriorTableName, FTableWarriorInfo& OutInfo) const
{
	if (!WarriorTableName.IsNone())
	{
		if (WarriorInfoTable)
		{
			FTableWarriorInfo* TableWarriorInfo;
			TableWarriorInfo = WarriorInfoTable->FindRow<FTableWarriorInfo>(WarriorTableName, "");

			if (TableWarriorInfo)
			{
				if (!TableWarriorInfo->WarriorInfo.DisplayName.IsNone())
				{
					OutInfo = *TableWarriorInfo;
					return true;
				}
			}
		}
	}

	return false;
}

bool UM2GameInstance::GetTableGridInfoByName(const FName GridTableName, FGridNodeInfo& OutInfo) const
{
	if (!GridTableName.IsNone())
	{
		if (GridInfoTable)
		{
			FGridNodeInfo* GridInfo;
			GridInfo = GridInfoTable->FindRow<FGridNodeInfo>(GridTableName, "");

			if (!GridInfo->DisplayName.IsNone())
			{
				OutInfo = *GridInfo;
				return true;
			}
		}
	}

	return false;
}
