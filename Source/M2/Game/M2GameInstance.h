// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Engine/DataTable.h"
#include "FuncLibrary/M2_Types.h"
#include "M2GameInstance.generated.h"

/**
 * 
 */
UCLASS()
class M2_API UM2GameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintPure, Category = "Table")
	bool GetTableWarriorInfoByName(const FName WarriorTableName, FTableWarriorInfo& OutInfo) const;

	UFUNCTION(BlueprintPure, Category = "Table")
	bool GetTableGridInfoByName(const FName GridTableName, FGridNodeInfo& OutInfo) const;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Table")
	UDataTable* WarriorInfoTable = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Table")
	UDataTable* GridInfoTable = nullptr;
};
