// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "M2GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class M2_API AM2GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
